﻿using System;

namespace Triangle
{
    class CalTriangle
    {

                

            class Global  
            {  
                public static double A;
                public static double B;
                public static double C;
                public static double area;
                public static double angleA;
                public static double angleB;
                 public static double angleC;
            
                
            } 
            public Double SetA(double num1) {

              
                      Global.A = num1;
                    return Global.A;
                

            }

             public Double SetC(double num1) {

              
                      Global.C = num1;
                    return Global.C;
                

            }
             public Double SetB(double num1) {

              
                      Global.B = num1;
                    return Global.B;
                

            }

                public Double CalculateA(double num1, double num2){  
                        double a;
                        Global.B = num1;
                        Global.C = num2;
                    
                        if (Global.B <= 0){

                            throw (new InvalidOperation("B has a no Value"));
                        }
                        else if (Global.C <= 0 ){

                            throw (new InvalidOperation("C has a no Value"));

                        }
                        else{
                            try{

                                a = Math.Sqrt( (num2 * num2) - (num1 * num1)); 
                                Global.A = a;
                                return Global.A;
                        
                            
                        }
                        catch(InvalidCastException e){

                            throw (new InvalidOperation("A has a no Value"));
                            
                        }
                     }

                    }
                
                public Double CalculateB(double num1, double num2){  
                        double b;
                        Global.A = num1;
                        Global.C = num2;
                    
                    if (Global.A <= 0){

                          throw (new InvalidOperation("A has a no Value"));
                    }
                    else if (Global.C <= 0 ){

                        throw (new InvalidOperation("C has a no Value"));

                    }
                    else{
                         try{

                            b = Math.Sqrt( (num2 * num2) - (num1 * num1)); 
                           Global.B = b;
                            return Global.B;
                       
                        
                    }
                    catch(InvalidCastException e){

                          throw (new InvalidOperation("A has a no Value"));
                       
                    }
                    }
                   
                }
                                    
              
              
                public Double CalculateC(double num1, double num2){  
                    double c;
                        Global.A = num1;
                        Global.B = num2;
                    
                    if (Global.A <= 0){

                          throw (new InvalidOperation("A has a no Value"));
                    }
                    else if (Global.C <= 0 ){

                        throw (new InvalidOperation("B has a no Value"));

                    }
                    else{
                         try{

                            c = Math.Sqrt((num1 * num1) + (num2 * num2)); 
                            Global.C = c;
                            return Global.C;
                       
                        
                    }
                    catch(InvalidCastException e){

                          throw (new InvalidOperation("A has a no Value"));
                       
                    }
                    }
                   
                }
                public static void CalculateArea(){  

                    try{

                        Global.area = ((Global.B * (Math.Sin(90) * Global.C)) / 2); 

                    }
                    catch(Exception e){

                        if(Global.A <= 0){

                             throw (new InvalidOperation("A has a no Value"));
                            
                        }
                        else if(Global.B <= 0){

                             throw (new InvalidOperation("B has a no Value"));
                             
                        }
                        else if(Global.C <= 0){

                             throw (new InvalidOperation("C has a no Value"));
                              

                        }

                    }
                    
                }


            public static void CalculateAngles(){


                    Global.angleA = Global.A / (Math.Asin(Global.C));
                    Global.angleB = Global.B / (Math.Acos(Global.C));
                    Global.angleC = 180 -(Global.angleA + Global.angleB);

            }




        
            public class OneNumberIsNull : Exception
            {
                public OneNumberIsNull(string message) : base(message)
                {
                }
            }

            public class AllNumbesAreNull : Exception
            {
                public AllNumbesAreNull(string message) : base(message)
                {
                }
            }

            public class InvalidOperation : Exception
            {
                public InvalidOperation(string message) : base(message)
                {
                }
            }
    }

  


   

    
}
