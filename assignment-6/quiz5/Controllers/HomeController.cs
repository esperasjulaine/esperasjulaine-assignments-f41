﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using quiz5.Models;
using MVCForm.Models;

namespace quiz5.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult registrationform()
        {
            return View();
        }

        [HttpPost] 
        public ActionResult Submit(Employee fc)
        {
            
            ViewBag.fName = fc.fName;
            ViewBag.lName = fc.lName;
            ViewBag.mName = fc.mName;
            ViewBag.age = fc.age;
            ViewBag.sex = fc.sex;
            ViewBag.address = fc.address;

            if(fc.sex == "Male"){

                ViewBag.pretext = "Mr.";
            }
            else
            {
                 ViewBag.pretext = "Mrs.";
            }
            


            return View("Index");
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
    
}
