using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace MVCForm.Models
{
    public class Employee
        {
            public string fName { get; set; }
            public string lName { get; set; }
            public string mName { get; set; }
            public int age { get; set; }
            public string sex { get; set; }
            public string address { get; set; }
            public string pretext { get; set; }

        }

}