﻿using System;
using System.Collections.Generic;  
using System.Linq;  
using System.Text;  
using System.Threading.Tasks; 

namespace Calculate
{
    //Class for Basic Calculation
    public class BasicCalculator {

              
            // Add
            
            public int Add(int num1, int num2){  
                return num1 + num2;  
            }  
        
            // Multiply 
            public int Multiply(int num1, int num2){  
                return num1 * num2;  
            }  
            // Subtract 
            public int Subtract(int num1, int num2){  
                    if (num1 > num2){  
                        return num1 - num2;  
                    }  
        
                    return num2 - num1;  
        
            }  
            //Division
            public float Division(float num1, float num2){  
                return num1 / num2;  
            }  
    }

    //Conversion from Decimal to Binary
    public class ConversionDecimalToBinary{

            
            public string DecimalToBinary(int num){
                 
                int arr;   
                string answer ="";
                for(int i=0; num>0; i++)      
                    {      
                    arr=num%2;      
                    num= num/2;    
                    answer += arr.ToString();
                    }      
                    
                             
                return answer;
            }
    } 

    //Class Scientific Calculation
    public class Scientific{

        
        public int Factorial(int num)
        {
            int x = 1;
            for (int y = 1; y <= num; y++){
               
                x = x * y;
            }
            return x;
        }

        public double Tangent(double bas){

            double tan = (bas * (Math.PI)) / 180;
            return Math.Tan(tan);
        }

        public double Cosine(double bas){

            double cosine = (bas * (Math.PI)) / 180;
            return Math.Cos(cosine);
        }

        public double Sine(double bas){

            double sine = (bas * (Math.PI)) / 180;
            return Math.Sin(sine);
        }

        

    }  
        
    

}
